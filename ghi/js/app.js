import * as snips from './modules/modules.js'


window.addEventListener('error', (urlEvent) =>{
    let wrapper = document.createElement('div')
    wrapper.innerHTML = urlEvent
});
onerror

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences'

    const data = await snips.getData(url)
    const conferences = data.conferences

    const cardTags = document.querySelectorAll('.col')
    Object.keys(conferences).forEach((x,i) => cardTags[i%3].innerHTML += snips.createPlaceholder())


    for (let ind in conferences){

        let conference = conferences[ind]
        let name = conference.name

        let confURL = `http://localhost:8000${conference.href}`
        let confData = await snips.getData(confURL)
        let description = confData.conference.description
        let start = new Date(confData.conference.starts).toLocaleDateString()
        let end = new Date(confData.conference.ends).toLocaleDateString()

        let locationURL = `http://localhost:8000${confData.conference.location.href}`
        let locationData = await snips.getData(locationURL)
        let picURL = locationData.picture_url
        let location = locationData.name

        let card = createCard(name, description, picURL, start, end, location)
        if (ind < 3){
            cardTags[ind%3].innerHTML = ''
        }
        cardTags[ind%3].innerHTML += card
    }

});


function createCard(name, description, picURL,start,end,location){
    return `
            <div class="card shadow mb-3">
                <img src="${picURL}" class="card-img-top" alt="...">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text"><small class="text-body-secondary">${location}</small></p>
                <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                   ${start} - ${end}
                </div>
            </div>
        `;
}
