import * as snips from './modules/modules.js'


window.addEventListener('DOMContentLoaded', async () =>{
    const url = 'http://localhost:8000/api/locations/'

    const data = await snips.getData(url)
    const selectLocationTag = document.getElementById('location')

    for (let location of data.locations ){
        let newOp = document.createElement("option")
        newOp.innerHTML = `${location.name}`
        newOp.setAttribute('value', location.id)
        selectLocationTag.appendChild(newOp)
    }

    const formTag = document.getElementById('create-conference-form')


    formTag.addEventListener('submit', async event =>{
        event.preventDefault();
        const formData = new FormData(formTag)
        const json = Object.fromEntries(formData)
        json['max_presentations'] = parseInt(json['max_presentations'])
        json['max_attendees'] = parseInt(json['max_attendees'])
        json['location'] = parseInt(json['location'])
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(json),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference)
        }


    });

});
